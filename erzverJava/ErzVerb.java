/*
** Beispiel: Erzeuger/Verbraucher-Problem als Monitor mit Java realisiert.
**
*/

/*
**************************************************************************
** Realisierung des beschränkten Puffers durch ein Array (Ringpuffer)
**  - wr: Schreibposition
**  - rd: Leseposition
*/

class Buffer {
	private int[] buf;
	private int wr;
	private int rd;
	private int size;

	public Buffer(int size) {
		buf = new int[size];
		wr = 0;
		rd = 0;
		this.size = size;
	}

	public void insertItem(int item) {
		buf[wr] = item;
		wr = (wr + 1) % size;
	}

	public int removeItem() {
		int res = buf[rd];
		rd = (rd + 1) % size;
		return res;
	}

	public int getSize() {
		return size;
	}
}

/*
 **************************************************************************
 ** Klassen für die Erzeuger- und Verbraucher-Threads
 */

class Producer extends Thread {
	private ErzVerb ev;
	private String name;

	public Producer(ErzVerb ev, String name) {
		this.ev = ev;
		this.name = name;
	}

	public void run() {
		int item = 0;
		int i;
		for (i = 0; i < 1000; i++) {
			item++; // Erzeuge item ...
			ev.insert(item);
		}
	}
}

class Consumer extends Thread {
	private ErzVerb ev;
	private String name;

	public Consumer(ErzVerb ev, String name) {
		this.ev = ev;
		this.name = name;
	}

	public void run() {
		int item;
		int i;
		for (i = 0; i < 1000; i++) {
			item = ev.remove(name); // Verbrauche item ...
		}
	}
}

public class ErzVerb {

	/*
	 **************************************************************************
	 ** Monitor für Erzeuger/Verbraucher-Problem
	 */

	/*
	 ** Hauptprogramm
	 */
	public static void main(String argv[]) {
		ErzVerb ev = new ErzVerb(1);

		/*
		 ** Erzeuge je einen Thread für die Prozedured Producer und Consumer
		 */
		Producer prod = new Producer(ev, "prod: ");
		Consumer cons = new Consumer(ev, "cons 1: ");
		Consumer cons2 = new Consumer(ev, "cons 2: ");

		prod.start();
		cons.start();
		cons2.start();
	}

	/*
	 ** Variable count zählt belegte Pufferplätze
	 */
	private int count;

	Buffer buffer;

	public ErzVerb(int size) {
		buffer = new Buffer(size);
		count = 0;
	}

	/*
	 ** Einfügeprozedur
	 */
	public synchronized void insert(int item) {
		System.out.println("betrete insert ");
		while (count == buffer.getSize()) {
			try {
				wait();
			} catch (InterruptedException ex) {
			}
		}
		buffer.insertItem(item);
		System.out.println("insert " + item);
		count++;
		notifyAll();
	}

	/*
	 ** Entfernprozedur
	 */
	public synchronized int remove(String name) {
		System.out.println("betrete remove " + name);
		int result;
		while (count == 0) {
			try {
				wait();
			} catch (InterruptedException ex) {
			}
		}
		result = buffer.removeItem();
		System.out.println("remove " + name + " " + result);
		count--;
		notifyAll();
		return result;
	}

}
